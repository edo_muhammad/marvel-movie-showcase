package com.core.domain.usecase.movieusecase

import com.core.domain.model.DomainApiResource
import com.core.repository.rawggamerepository.IMovieRepository
import com.core.ui.mapper.MovieDomainToUiMapper
import com.core.ui.model.FeatureMovieUIModel
import com.core.ui.model.MovieUiModel
import com.core.ui.model.UiState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class GetMovieListUseCase @Inject constructor(private val movieRepository: IMovieRepository) {

    operator fun invoke(): Flow<UiState<FeatureMovieUIModel>> = flow {
        emit(UiState.OnLoading())

        val resultLatestMovies = movieRepository.fetchMovies(1, "2023")
        val resultLatestSeries = movieRepository.fetchMovies(2)
        val resultTrending = movieRepository.fetchMovies(1)

        when (resultLatestMovies) {
            is DomainApiResource.SuccessResource -> {
                val movieUIModelsLatestMovies: List<MovieUiModel> =
                    resultLatestMovies.data.results.map {
                        MovieDomainToUiMapper().mapToUIModel(it)
                    }
                val movieUIModelsLatestSeries: List<MovieUiModel> =
                    (resultLatestSeries as? DomainApiResource.SuccessResource)?.data?.results?.map {
                        MovieDomainToUiMapper().mapToUIModel(it)
                    } ?: emptyList()
                val movieUIModelsTrending: List<MovieUiModel> =
                    (resultTrending as? DomainApiResource.SuccessResource)?.data?.results?.map {
                        MovieDomainToUiMapper().mapToUIModel(it)
                    } ?: emptyList()

                val featureMovieUIModel = FeatureMovieUIModel(
                    latestMovies = movieUIModelsLatestMovies,
                    latestSeries = movieUIModelsLatestSeries,
                    trending = movieUIModelsTrending
                )

                emit(UiState.OnSuccess(featureMovieUIModel))
            }

            is DomainApiResource.ErrorResource -> {
                emit(UiState.OnFailed(resultLatestMovies.message))
            }

            is DomainApiResource.ExceptionResource -> {
                emit(UiState.OnFailed(resultLatestMovies.e.message.orEmpty()))
            }

            is DomainApiResource.SuccessNoDataResource -> Unit
        }
    }.flowOn(Dispatchers.IO)
}