package com.core.domain.usecase.movieusecase

import com.core.domain.model.DomainApiResource
import com.core.repository.rawggamerepository.IMovieRepository
import com.core.ui.mapper.MovieDomainToUiMapper
import com.core.ui.model.MovieUiModel
import com.core.ui.model.UiState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class GetMovieUseCase @Inject constructor(private val movieRepository: IMovieRepository) {

    operator fun invoke(imdbId: String): Flow<UiState<MovieUiModel>> = flow<UiState<MovieUiModel>> {
        emit(UiState.OnLoading())

        when (val resultLatestMovies = movieRepository.fetchMovie(imdbId)) {
            is DomainApiResource.SuccessResource -> {
                emit(UiState.OnSuccess(MovieDomainToUiMapper().mapToUIModel(resultLatestMovies.data)))
            }

            is DomainApiResource.ErrorResource -> {
                emit(UiState.OnFailed(resultLatestMovies.message))
            }

            is DomainApiResource.ExceptionResource -> {
                emit(UiState.OnFailed(resultLatestMovies.e.message.orEmpty()))
            }

            is DomainApiResource.SuccessNoDataResource -> Unit
        }
    }.flowOn(Dispatchers.IO)
}