package com.core.common

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Date

fun String.resolveMovieSmallImgUrl(): String {
    return BuildConfig.MOVIE_PHOTO_SMALL_BASE_URL + this
}

fun String.resolveMovieLargeImgUrl(): String {
    return BuildConfig.MOVIE_PHOTO_ORIGINAL_BASE_URL + this
}

fun String.resolveYoutubeThumbnailUrl(): String {
    return BuildConfig.YOUTUBE_THUMBNAIL.replace("{ID}", this)
}

fun watchYoutubeVideo(context: Context, id: String) {
    val appIntent = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:$id"))
    val webIntent = Intent(
        Intent.ACTION_VIEW,
        Uri.parse("http://www.youtube.com/watch?v=$id")
    )
    try {
        context.startActivity(appIntent)
    } catch (ex: ActivityNotFoundException) {
        context.startActivity(webIntent)
    }
}

fun String.formatToDdMmYyyy(): String {
    val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    val outputFormat = SimpleDateFormat("dd-MM-yyyy")

    return try {
        val date: Date = inputFormat.parse(this)
        val formattedDate: String = outputFormat.format(date)

        formattedDate
    } catch (e: ParseException) {
        e.printStackTrace()

        this
    }
}