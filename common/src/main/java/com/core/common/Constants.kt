package com.core.common

object Constants {
    const val MOVIE_BASE_URL = "MOVIE_BASE_URL"
    const val MOVIE_OK_HTTP = "MOVIE_OK_HTTP"
    const val MOVIE_RETROFIT = "MOVIE_RETROFIT"
    const val PARAM_PAGE = "page"
    const val YEAR = "year"
    const val ID = "i"
    const val PAGE_SIZE = 30
    const val STARTING_KEY = 1
}