package com.core.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.core.dto.model.local.MovieLocalDto
import com.core.local.db.dao.MovieDao

@Database(
    entities = [MovieLocalDto::class],
    version = 1,
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getMovieDao(): MovieDao
}