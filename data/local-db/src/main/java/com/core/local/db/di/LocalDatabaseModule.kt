package com.core.local.db.di

import android.content.Context
import androidx.room.Room
import com.core.local.db.AppDatabase
import com.core.local.db.dao.MovieDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class LocalDatabaseModule {

    @Singleton
    @Provides
    fun provideGamesDatabase(@ApplicationContext context: Context): AppDatabase =
        Room.databaseBuilder(context, AppDatabase::class.java, "DB_NAME")
            .fallbackToDestructiveMigration()
            .build()

    @Singleton
    @Provides
    fun providesMovieDao(appDatabase: AppDatabase): MovieDao =
        appDatabase.getMovieDao()
}