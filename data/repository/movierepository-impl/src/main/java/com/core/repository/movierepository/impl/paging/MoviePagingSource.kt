package com.core.repository.movierepository.impl.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.core.common.Constants.STARTING_KEY
import com.core.data.model.ApiResponse
import com.core.domain.mapper.MovieResponseRemoteDtoToDomainMapper
import com.core.domain.model.DomainApiResource
import com.core.domain.model.MovieDomainModel
import com.core.dto.model.remote.MovieResponseRemoteDto
import com.core.repository.lib.mapApiResponseToDomain
import java.io.IOException

class MoviePagingSource(private val transform: suspend (page: Int) -> ApiResponse<MovieResponseRemoteDto>) :
    PagingSource<Int, MovieDomainModel>() {

    override fun getRefreshKey(state: PagingState<Int, MovieDomainModel>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, MovieDomainModel> {
        val startKey = params.key ?: STARTING_KEY

        return try {
            val response = transform.invoke(startKey)

            return when (val apiResponse =
                response.mapApiResponseToDomain(MovieResponseRemoteDtoToDomainMapper())) {

                is DomainApiResource.ErrorResource, is DomainApiResource.ExceptionResource -> {
                    LoadResult.Error(Throwable("Error occurred"))
                }

                is DomainApiResource.SuccessResource -> {
                    val endOfPagination = apiResponse.data.results.isEmpty()

                    val prevKey = if (startKey == 1) null else startKey - 1
                    val nextKey = if (endOfPagination) null else startKey + 1

                    LoadResult.Page(
                        data = apiResponse.data.results,
                        prevKey = prevKey,
                        nextKey = nextKey
                    )
                }

                is DomainApiResource.SuccessNoDataResource -> {
                    LoadResult.Page(
                        data = emptyList(),
                        prevKey = null,
                        nextKey = null
                    )
                }
            }
        } catch (e: IOException) {
            LoadResult.Error(e)
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }
}