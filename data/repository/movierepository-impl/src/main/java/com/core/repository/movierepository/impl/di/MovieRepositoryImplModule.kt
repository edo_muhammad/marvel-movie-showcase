package com.core.repository.movierepository.impl.di

import com.core.datasource.remote.MovieRemoteDataSource
import com.core.repository.movierepository.impl.MovieRepositoryImpl
import com.core.repository.rawggamerepository.IMovieRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class MovieRepositoryImplModule {
    @Singleton
    @Provides
    fun provideRawgGameLocalDataSource(
        movieRemoteDataSource: MovieRemoteDataSource
    ): IMovieRepository =
        MovieRepositoryImpl(movieRemoteDataSource)
}