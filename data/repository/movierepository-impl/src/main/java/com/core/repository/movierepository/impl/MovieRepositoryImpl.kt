package com.core.repository.movierepository.impl

import com.core.datasource.remote.MovieRemoteDataSource
import com.core.domain.mapper.MovieRemoteDtoToDomainMapper
import com.core.domain.mapper.MovieResponseRemoteDtoToDomainMapper
import com.core.domain.model.DomainApiResource
import com.core.domain.model.MovieDomainModel
import com.core.domain.model.MovieResponseDomainModel
import com.core.repository.lib.mapApiResponseToDomain
import com.core.repository.rawggamerepository.IMovieRepository
import javax.inject.Inject

class MovieRepositoryImpl @Inject constructor(
    private val movieRemoteDataSource: MovieRemoteDataSource
) : IMovieRepository {
    override suspend fun fetchMovies(page: Int, year: String): DomainApiResource<MovieResponseDomainModel> {
        return movieRemoteDataSource.fetchMovieList(page, year)
            .mapApiResponseToDomain(MovieResponseRemoteDtoToDomainMapper())
    }

    override suspend fun fetchMovie(id: String): DomainApiResource<MovieDomainModel> {
        return movieRemoteDataSource.fetchMovie(id).mapApiResponseToDomain(
            MovieRemoteDtoToDomainMapper()
        )
    }
}