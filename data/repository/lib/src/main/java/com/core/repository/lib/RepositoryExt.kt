package com.core.repository.lib

import com.core.data.model.ApiError
import com.core.data.model.ApiException
import com.core.data.model.ApiResponse
import com.core.data.model.ApiSuccess
import com.core.domain.mapper.base.BaseRemoteDtoToDomainMapper
import com.core.domain.model.DomainApiResource
import com.core.domain.model.DomainModel
import com.core.dto.model.remote.RemoteDtoModel

fun <V : RemoteDtoModel, T : DomainModel> ApiResponse<V>.mapApiResponseToDomain(domainMapper: BaseRemoteDtoToDomainMapper<V, T>): DomainApiResource<T> {
    return when (this) {
        is ApiError -> DomainApiResource.ErrorResource(this.code, this.message)
        is ApiException -> DomainApiResource.ExceptionResource(this.exception)
        is ApiSuccess -> {
            DomainApiResource.SuccessResource(domainMapper.mapToDomainModel(this.data))
        }
    }
}