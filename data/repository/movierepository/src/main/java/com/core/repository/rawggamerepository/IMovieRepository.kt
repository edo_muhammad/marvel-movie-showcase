package com.core.repository.rawggamerepository

import com.core.domain.model.DomainApiResource
import com.core.domain.model.MovieDomainModel
import com.core.domain.model.MovieResponseDomainModel

interface IMovieRepository {
    suspend fun fetchMovies(page: Int, year: String = ""): DomainApiResource<MovieResponseDomainModel>

    suspend fun fetchMovie(id: String): DomainApiResource<MovieDomainModel>
}