package com.core.datasource.remote

import com.core.api.service.MovieService
import com.core.common.Constants
import com.core.common.Constants.YEAR
import com.core.data.model.ApiResponse
import com.core.datasource.remote.utils.PARAM_GENRE
import com.core.datasource.remote.utils.handleRetrofitResponse
import com.core.dto.model.remote.MovieRemoteDto
import com.core.dto.model.remote.MovieResponseRemoteDto
import java.time.Year
import javax.inject.Inject

class MovieRemoteDataSource @Inject constructor(private val movieService: MovieService) {
    suspend fun fetchMovieList(page: Int, year: String): ApiResponse<MovieResponseRemoteDto> {
        return handleRetrofitResponse {
            movieService.fetchMovies(
                hashMapOf(
                    Constants.PARAM_PAGE to page.toString(),
                    YEAR to year
                )
            )
        }
    }

    suspend fun fetchMovie(id: String): ApiResponse<MovieRemoteDto> {
        return handleRetrofitResponse {
            movieService.fetchMovie(
                hashMapOf(
                    Constants.ID to id
                )
            )
        }
    }
}