package com.core.datasource.remote.di

import com.core.api.service.MovieService
import com.core.datasource.remote.MovieRemoteDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RemoteDataSourceModule {
    @Singleton
    @Provides
    fun providesMovieRemoteDataSource(movieService: MovieService): MovieRemoteDataSource =
        MovieRemoteDataSource(movieService)
}