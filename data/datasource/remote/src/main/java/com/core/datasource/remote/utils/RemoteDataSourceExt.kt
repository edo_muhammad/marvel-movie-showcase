package com.core.datasource.remote.utils

import com.core.data.model.ApiError
import com.core.data.model.ApiException
import com.core.data.model.ApiResponse
import com.core.data.model.ApiSuccess
import com.core.dto.model.remote.RemoteDtoModel
import retrofit2.HttpException
import retrofit2.Response

suspend fun <T : Any> handleRetrofitResponse(
    transform: suspend () -> Response<T>
): ApiResponse<T> {
    return try {
        val response = transform.invoke()
        val body = response.body()

        if (response.isSuccessful && body != null) {
            ApiSuccess(body)
        } else {
            ApiError(code = response.code(), message = response.message())
        }
    } catch (e: HttpException) {
        ApiError(code = e.code(), message = e.message())
    } catch (e: Throwable) {
        ApiException(e)
    }
}
