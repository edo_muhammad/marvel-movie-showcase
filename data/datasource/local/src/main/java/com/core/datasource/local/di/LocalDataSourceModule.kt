package com.core.datasource.local.di

import com.core.datasource.local.MovieLocalDataSource
import com.core.local.db.dao.MovieDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class LocalDataSourceModule {

    @Singleton
    @Provides
    fun provideRawgGameLocalDataSource(movieDAO: MovieDao): MovieLocalDataSource =
        MovieLocalDataSource()
}