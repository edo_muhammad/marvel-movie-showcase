package com.core.api.service.di

import com.core.api.service.MovieService
import com.core.common.Constants.MOVIE_RETROFIT
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ApiServiceModule {

    @Provides
    @Singleton
    fun providesMovieService(@Named(MOVIE_RETROFIT) retrofit: Retrofit): MovieService =
        retrofit.create(MovieService::class.java)

}