package com.core.api.service

import com.core.dto.model.remote.MovieRemoteDto
import com.core.dto.model.remote.MovieResponseRemoteDto
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.QueryMap

interface MovieService {
    @GET("?s=Marvel&r=json")
    suspend fun fetchMovies(@QueryMap queries: Map<String, String>): Response<MovieResponseRemoteDto>

    @GET("?r=json")
    suspend fun fetchMovie(@QueryMap queries: Map<String, String>): Response<MovieRemoteDto>
}