package com.core.ui.mapper

import com.core.domain.model.MovieDomainModel
import com.core.ui.mapper.base.BaseDomainToUiMapper
import com.core.ui.model.MovieUiModel

class MovieDomainToUiMapper : BaseDomainToUiMapper<MovieDomainModel, MovieUiModel> {
    override fun mapToUIModel(domainEntity: MovieDomainModel): MovieUiModel =
        MovieUiModel(
            id = domainEntity.id,
            title = domainEntity.title,
            year = domainEntity.year,
            type = domainEntity.type,
            posterImgUrl = domainEntity.posterImgUrl,
            metaScore = domainEntity.metaScore,
            imgDbRating = domainEntity.imgDbRating,
            imdbVotes = domainEntity.imdbVotes,
            ratings = domainEntity.ratings.map {
                RatingDomainToUiMapper().mapToUIModel(it)
            }
        )
}