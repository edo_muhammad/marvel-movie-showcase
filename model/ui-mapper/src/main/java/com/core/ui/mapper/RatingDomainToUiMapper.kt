package com.core.ui.mapper

import com.core.domain.model.RatingDomainModel
import com.core.ui.mapper.base.BaseDomainToUiMapper
import com.core.ui.model.RatingUiModel

class RatingDomainToUiMapper :
    BaseDomainToUiMapper<RatingDomainModel, RatingUiModel> {
    override fun mapToUIModel(domainEntity: RatingDomainModel): RatingUiModel {
        return RatingUiModel(
            source = domainEntity.source,
            value = domainEntity.value
        )
    }
}