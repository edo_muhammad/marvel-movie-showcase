package com.core.ui.mapper.base

import com.core.ui.model.UiModel
import com.core.domain.model.DomainModel

interface BaseDomainToUiMapper<V : DomainModel, T : UiModel> {
	fun mapToUIModel(domainEntity: V): T
}