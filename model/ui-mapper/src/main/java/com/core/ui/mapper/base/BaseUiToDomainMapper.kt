package com.core.ui.mapper.base

import com.core.ui.model.UiModel
import com.core.domain.model.DomainModel

interface BaseUiToDomainMapper<V : UiModel, T : DomainModel> {
    fun mapToDomainModel(uiModel: V): T
}