package com.core.domain.model

sealed class DomainApiResource<T> {
    data class SuccessResource<T>(val data: T) : DomainApiResource<T>()
    data class ErrorResource<T>(val code: Int, val message: String) : DomainApiResource<T>()
    data class ExceptionResource<T>(val e: Throwable) : DomainApiResource<T>()
    class SuccessNoDataResource<T> : DomainApiResource<T>()
}