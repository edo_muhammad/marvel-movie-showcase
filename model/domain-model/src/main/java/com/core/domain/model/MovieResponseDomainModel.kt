package com.core.domain.model

data class MovieResponseDomainModel(

    val results: List<MovieDomainModel>

) : DomainModel()