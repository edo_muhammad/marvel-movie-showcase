package com.core.domain.model

data class MovieDomainModel(

    val id: String,

    val title: String,

    val year: String,

    val type: String,

    val posterImgUrl: String,

    val metaScore: String,

    val imgDbRating: String,

    val imdbVotes: String,

    val ratings: List<RatingDomainModel>
) : DomainModel()