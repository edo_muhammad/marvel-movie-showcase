package com.core.domain.model

data class RatingDomainModel(
    val source: String,

    val value: String
) : DomainModel()