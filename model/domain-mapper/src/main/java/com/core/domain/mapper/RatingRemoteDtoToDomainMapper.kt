package com.core.domain.mapper

import com.core.domain.mapper.base.BaseRemoteDtoToDomainMapper
import com.core.domain.model.RatingDomainModel
import com.core.dto.model.remote.RatingRemoteDto

class RatingRemoteDtoToDomainMapper :
    BaseRemoteDtoToDomainMapper<RatingRemoteDto, RatingDomainModel> {
    override fun mapToDomainModel(dataClass: RatingRemoteDto): RatingDomainModel =
        RatingDomainModel(
            source = dataClass.source.orEmpty(),
            value = dataClass.value.orEmpty()
        )
}