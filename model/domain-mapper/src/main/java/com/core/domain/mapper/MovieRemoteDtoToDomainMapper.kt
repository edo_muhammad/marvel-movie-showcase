package com.core.domain.mapper

import com.core.domain.mapper.base.BaseRemoteDtoToDomainMapper
import com.core.domain.model.MovieDomainModel
import com.core.dto.model.remote.MovieRemoteDto

class MovieRemoteDtoToDomainMapper : BaseRemoteDtoToDomainMapper<MovieRemoteDto, MovieDomainModel> {
    override fun mapToDomainModel(dataClass: MovieRemoteDto): MovieDomainModel =
        MovieDomainModel(
            id = dataClass.id.orEmpty(),
            title = dataClass.title.orEmpty(),
            year = dataClass.year.orEmpty(),
            posterImgUrl = dataClass.posterImgUrl.orEmpty(),
            type = dataClass.type.orEmpty(),
            metaScore = dataClass.metaScore.orEmpty(),
            imgDbRating = dataClass.imgDbRating.orEmpty(),
            imdbVotes = dataClass.imdbVotes.orEmpty(),
            ratings = dataClass.ratings?.map {
                RatingRemoteDtoToDomainMapper().mapToDomainModel(it)
            } ?: emptyList()
        )
}