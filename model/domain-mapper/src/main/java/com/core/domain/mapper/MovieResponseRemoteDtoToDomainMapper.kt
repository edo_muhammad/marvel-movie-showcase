package com.core.domain.mapper

import com.core.domain.mapper.base.BaseRemoteDtoToDomainMapper
import com.core.domain.model.MovieResponseDomainModel
import com.core.dto.model.remote.MovieResponseRemoteDto

class MovieResponseRemoteDtoToDomainMapper :
    BaseRemoteDtoToDomainMapper<MovieResponseRemoteDto, MovieResponseDomainModel> {
    override fun mapToDomainModel(dataClass: MovieResponseRemoteDto): MovieResponseDomainModel =
        MovieResponseDomainModel(
            results = dataClass.results?.map {
                MovieRemoteDtoToDomainMapper().mapToDomainModel(it)
            }.orEmpty()
        )
}