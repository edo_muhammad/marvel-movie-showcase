package com.core.domain.mapper.base

import com.core.domain.model.DomainModel
import com.core.dto.model.remote.RemoteDtoModel

/*
* Base mapper for mapping data class to domain class
*/
interface BaseRemoteDtoToDomainMapper<V : RemoteDtoModel, T : DomainModel> {
    fun mapToDomainModel(dataClass: V): T
}