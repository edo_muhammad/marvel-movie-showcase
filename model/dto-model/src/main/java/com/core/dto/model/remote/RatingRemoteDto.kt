package com.core.dto.model.remote

import com.google.gson.annotations.SerializedName

data class RatingRemoteDto(
    @SerializedName("Source")
    val source: String? = null,

    @SerializedName("Value")
    val value: String? = null
) : RemoteDtoModel()