package com.core.dto.model.local

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MovieLocalDto(
    @PrimaryKey(autoGenerate = true)
    val myKey: Int
)