package com.core.dto.model.remote

import com.google.gson.annotations.SerializedName

data class MovieResponseRemoteDto(

    @SerializedName("Search")
    val results: List<MovieRemoteDto>? = null

) : RemoteDtoModel()