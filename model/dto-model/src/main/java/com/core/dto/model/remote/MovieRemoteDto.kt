package com.core.dto.model.remote

import com.google.gson.annotations.SerializedName


data class MovieRemoteDto(

    @SerializedName("imdbID")
    val id: String? = null,

    @SerializedName("Title")
    val title: String? = null,

    @SerializedName("Year")
    val year: String? = null,

    @SerializedName("Type")
    val type: String? = null,

    @SerializedName("Poster")
    val posterImgUrl: String? = null,

    @SerializedName("Metascore")
    val metaScore: String? = null,

    @SerializedName("imdbRating")
    val imgDbRating: String? = null,

    @SerializedName("imdbVotes")
    val imdbVotes: String? = null,

    @SerializedName("Ratings")
    val ratings: List<RatingRemoteDto>? = null
) : RemoteDtoModel()