package com.core.data.model

class ApiError<T : Any>(val code: Int, val message: String) : ApiResponse<T>