package com.core.data.model

class ApiException<T : Any>(val exception: Throwable) : ApiResponse<T>