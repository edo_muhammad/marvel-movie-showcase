package com.core.data.model

data class ApiSuccess<T : Any>(val data: T) : ApiResponse<T>