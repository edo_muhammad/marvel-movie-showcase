package com.core.ui.model

import java.io.Serializable

data class RatingUiModel(
    val source: String,

    val value: String
) : UiModel(), Serializable