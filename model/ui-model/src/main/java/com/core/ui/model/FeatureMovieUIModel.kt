package com.core.ui.model

data class FeatureMovieUIModel(
    val latestMovies: List<MovieUiModel>,

    val latestSeries: List<MovieUiModel>,

    val trending: List<MovieUiModel>
)