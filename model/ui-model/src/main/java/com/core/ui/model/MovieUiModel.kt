package com.core.ui.model

import java.io.Serializable

data class MovieUiModel(

    val id: String,

    val title: String,

    val year: String,

    val type: String,

    val posterImgUrl: String,

    val metaScore: String,

    val imgDbRating: String,

    val imdbVotes: String,

    val ratings: List<RatingUiModel>

) : UiModel(), Serializable