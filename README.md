# Movie Showcase

This app shows list of popular movies

## Getting started

This project uses Modular MVVM + Clean Architecture. There are 4 main modules in this project: data, model, domain, and feature.

- Module data represents data from either api or local
- Module model represents model like remote dto, local dto, domain model, ui model
- Module domain is for a bridge between repository (data) and viewmodel (feature)
- Module feature represents ui (viewmodels, activity, fragments, views, adapters, etc)

This app uses api from rapidapi

## Languange Programming

- Kotlin version 1.8.20 

## Library Used

This project uses some libraries like 

- Retrofit
- Jetpack Navigation
- Coroutine
- Room 
- Glide 
- Hilt Dependency Injection 
