package com.movie.showcase.core

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

const val API_HEADER = "X-RapidAPI-Key"
const val API_HOST = "X-RapidAPI-Host"

class MovieInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()

        val requestBuilder: Request.Builder = originalRequest.newBuilder()
            .addHeader(API_HEADER, ApiKeys.movieToken())
            .addHeader(API_HOST, ApiKeys.movieApiHost())

        return chain.proceed(requestBuilder.build())
    }
}