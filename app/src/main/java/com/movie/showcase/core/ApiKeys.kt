package com.movie.showcase.core

object ApiKeys {

	init {
		System.loadLibrary("native-lib")
	}

	external fun movieToken(): String

	external fun movieApiHost(): String
}