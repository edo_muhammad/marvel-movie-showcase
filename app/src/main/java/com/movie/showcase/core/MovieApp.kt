package com.movie.showcase.core

import android.app.Application
import dagger.hilt.android.HiltAndroidApp
@HiltAndroidApp
class MovieApp : Application()