package com.movie.showcase.epoxy.controller

import android.view.View
import com.airbnb.epoxy.EpoxyAsyncUtil
import com.airbnb.epoxy.EpoxyController
import com.airbnb.epoxy.carousel
import com.airbnb.epoxy.group
import com.core.ui.model.MovieUiModel
import com.movie.showcase.R
import com.movie.showcase.epoxy.LargeHeroModel_
import com.movie.showcase.epoxy.overline
import java.util.concurrent.CopyOnWriteArrayList
import kotlin.random.Random

class FeatureController : EpoxyController(
    EpoxyAsyncUtil.getAsyncBackgroundHandler(),
    EpoxyAsyncUtil.getAsyncBackgroundHandler()
) {
    private val _latestMovies: CopyOnWriteArrayList<LargeHeroModel_> = CopyOnWriteArrayList()
    private val _latestSeries: CopyOnWriteArrayList<LargeHeroModel_> = CopyOnWriteArrayList()
    private val _trendingToday: CopyOnWriteArrayList<LargeHeroModel_> = CopyOnWriteArrayList()

    var listener: ((movieUiModel: MovieUiModel) -> Unit)? = null

    fun submitTrendingToday(items: Collection<MovieUiModel>) {
        _trendingToday.clear()
        _trendingToday.addAll(items.mapMovieUiToEpoxyModel())
        requestModelBuild()
    }

    fun submitLatestSeries(items: Collection<MovieUiModel>) {
        _latestSeries.clear()
        _latestSeries.addAll(items.mapMovieUiToEpoxyModel())
        requestModelBuild()
    }

    fun submitLatestMovies(items: Collection<MovieUiModel>) {
        _latestMovies.clear()
        _latestMovies.addAll(items.mapMovieUiToEpoxyModel())
        requestModelBuild()
    }

    private fun Collection<MovieUiModel>.mapMovieUiToEpoxyModel(): Collection<LargeHeroModel_> {
        val clickListener: ((movieUiModel: MovieUiModel) -> Unit)? = listener

        return this.map {
            LargeHeroModel_()
                .id("featured:" + it.id + Random(10).nextInt())
                .imageUrl(it.posterImgUrl).apply {
                    this.clickListener = object : View.OnClickListener {
                        override fun onClick(v: View?) {
                            clickListener?.invoke(it)
                        }
                    }
                }
        }
    }

    override fun buildModels() {
        if (_latestMovies.isNotEmpty()) {
            overline {
                id("featured:overline-1")
                value("Latest Movies")
            }

            val items: CopyOnWriteArrayList<LargeHeroModel_> = _latestMovies

            group {
                id("group:carousel-latest-movies")
                layout(R.layout.component_layout_nested_scrollable_host)
                carousel {
                    id("carousel-latest-movies")
                    models(items)
                    numViewsToShowOnScreen(2.1f)
                }
            }

        }

        if (_latestSeries.isNotEmpty()) {
            overline {
                id("featured:overline-2")
                value("Latest Series")
            }

            val items: CopyOnWriteArrayList<LargeHeroModel_> = _latestSeries

            group {
                id("group:carousel-latest-series")
                layout(R.layout.component_layout_nested_scrollable_host)
                carousel {
                    id("carousel-latest-series")
                    models(items)
                    numViewsToShowOnScreen(2.1f)
                }
            }
        }

        if (_trendingToday.isNotEmpty()) {
            overline {
                id("featured:overline-3")
                value("Trending Today")
            }

            val items: CopyOnWriteArrayList<LargeHeroModel_> = _trendingToday

            group {
                id("group:carousel-trending")
                layout(R.layout.component_layout_nested_scrollable_host)
                carousel {
                    id("carousel-trending")
                    models(items)
                    numViewsToShowOnScreen(3.1f)
                }
            }
        }
    }
}