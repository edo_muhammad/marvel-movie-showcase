package com.movie.showcase.epoxy

import android.view.View
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.movie.showcase.R
import com.movie.showcase.databinding.ComponentLargeOverlineBinding

@EpoxyModelClass
abstract class LargeOverlineModel : EpoxyModelWithHolder<LargeOverlineModel.LargeOverlineHolder>() {

    @field:EpoxyAttribute
    open var value: CharSequence? = null

    override fun getDefaultLayout(): Int {
        return R.layout.component_large_overline
    }

    override fun bind(holder: LargeOverlineHolder) {
        holder.binding.root.text = value
    }

    class LargeOverlineHolder : EpoxyHolder() {
        lateinit var binding: ComponentLargeOverlineBinding
            private set

        override fun bindView(itemView: View) {
            binding = ComponentLargeOverlineBinding.bind(itemView)
        }
    }
}