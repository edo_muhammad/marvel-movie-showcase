package com.movie.showcase.epoxy

import android.content.Context
import android.util.AttributeSet
import com.airbnb.epoxy.ModelView
import com.google.android.material.card.MaterialCardView

@ModelView(autoLayout = ModelView.Size.MATCH_WIDTH_WRAP_HEIGHT)
class FailureModelView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : MaterialCardView(context, attrs) {

    init {
        inflate(context, com.movie.showcase.R.layout.component_alert, this)
    }

}