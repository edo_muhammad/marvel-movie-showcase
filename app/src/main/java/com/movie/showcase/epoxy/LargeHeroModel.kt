package com.movie.showcase.epoxy

import android.view.View
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.bumptech.glide.Glide
import com.movie.showcase.R
import com.movie.showcase.databinding.ComponentLargeHeroBinding

@EpoxyModelClass
abstract class LargeHeroModel() : EpoxyModelWithHolder<LargeHeroModel.LargeHeroHolder>() {

    @field:EpoxyAttribute
    open var imageUrl: String? = null

    @field:EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    open var clickListener: View.OnClickListener? = null

    override fun getDefaultLayout(): Int {
        return R.layout.component_large_hero
    }

    override fun bind(holder: LargeHeroHolder) {
        with(holder.binding) {
            val imageView = imageView
            Glide.with(imageView).load(imageUrl)
                .fitCenter()
                .centerCrop()
                .placeholder(R.drawable.placeholder)
                .into(imageView)
            container.setOnClickListener(clickListener)
        }
    }

    class LargeHeroHolder : EpoxyHolder() {
        lateinit var binding: ComponentLargeHeroBinding
            private set

        override fun bindView(itemView: View) {
            binding = ComponentLargeHeroBinding.bind(itemView)
        }
    }
}