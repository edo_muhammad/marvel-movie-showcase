package com.movie.showcase.epoxy

import android.view.View
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.movie.showcase.R
import com.movie.showcase.databinding.ComponentOverlineBinding

@EpoxyModelClass
abstract class OverlineModel : EpoxyModelWithHolder<OverlineModel.OverlineHolder>() {

    @field:EpoxyAttribute
    open var value: CharSequence? = null

    override fun getDefaultLayout(): Int {
        return R.layout.component_overline
    }

    override fun bind(holder: OverlineHolder) {
        holder.binding.root.text = value
    }
    class OverlineHolder : EpoxyHolder() {
        lateinit var binding: ComponentOverlineBinding
            private set

        override fun bindView(itemView: View) {
            binding = ComponentOverlineBinding.bind(itemView)
        }
    }
}