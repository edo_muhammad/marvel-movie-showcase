package com.movie.showcase.epoxy

import android.view.View
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.movie.showcase.R
import com.movie.showcase.databinding.ComponentWideButtonBinding

@EpoxyModelClass
abstract class WideButtonModel : EpoxyModelWithHolder<WideButtonModel.WideButtonHolder>() {

    @field:EpoxyAttribute
    open var text: CharSequence? = null

    override fun getDefaultLayout(): Int {
        return R.layout.component_wide_button
    }

    override fun bind(holder: WideButtonHolder) {
        holder.binding.root.text = text
    }

    class WideButtonHolder : EpoxyHolder() {
        lateinit var binding: ComponentWideButtonBinding
            private set

        override fun bindView(itemView: View) {
            binding = ComponentWideButtonBinding.bind(itemView)
        }
    }
}