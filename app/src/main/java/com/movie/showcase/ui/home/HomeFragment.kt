package com.movie.showcase.ui.home


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.movie.showcase.databinding.FragmentHomeBinding
import com.movie.showcase.epoxy.controller.FeatureController
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding: FragmentHomeBinding get() = _binding!!
    private val viewModel: HomeViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)

        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val controller = FeatureController()
        binding.epoxyRecyclerView.setController(controller)

        controller.listener = {
            findNavController().navigate(HomeFragmentDirections.openMovieDetail(it))
        }

        viewModel.getMovies()

        viewModel.featureMovie.observe(viewLifecycleOwner) {
            controller.submitLatestMovies(it.latestMovies)
            controller.submitLatestSeries(it.latestSeries)
            controller.submitTrendingToday(it.trending)
        }

        viewModel.error.observe(viewLifecycleOwner) {
            context?.let { context ->
                Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}