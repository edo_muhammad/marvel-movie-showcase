package com.movie.showcase.ui.detail

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.core.domain.usecase.movieusecase.GetMovieUseCase
import com.core.ui.model.MovieUiModel
import com.core.ui.model.UiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(val getMovieUseCase: GetMovieUseCase) : ViewModel() {

    private val _movieUiModel: MutableLiveData<MovieUiModel> = MutableLiveData()
    val movieUiModel: LiveData<MovieUiModel> = _movieUiModel

    private val _error: MutableLiveData<String> = MutableLiveData()
    val error: LiveData<String> = _error

    val progressVisibility: MutableLiveData<Int> = MutableLiveData(View.VISIBLE)

    fun fetchMovie(imdbId: String) = viewModelScope.launch(Dispatchers.Main) {
        getMovieUseCase.invoke(imdbId).collect {
            when (it) {
                is UiState.OnFailed -> {
                    _error.value = it.message
                }

                is UiState.OnLoading -> {
                    progressVisibility.value = View.VISIBLE
                }

                is UiState.OnSuccess -> {
                    progressVisibility.value = View.GONE

                    _movieUiModel.value = it.data
                }
            }
        }
    }
}