package com.movie.showcase.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.movie.showcase.R
import com.movie.showcase.databinding.FragmentDetailBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailFragment : Fragment() {

    private var _binding: FragmentDetailBinding? = null
    private val binding: FragmentDetailBinding get() = _binding!!

    val args: DetailFragmentArgs by navArgs()

    private val viewModel: DetailViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailBinding.inflate(inflater, container, false)

        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.fetchMovie(args.movieUiModel.id.toString())

        viewModel.movieUiModel.observe(viewLifecycleOwner) {
            with(binding) {
                val movieUiModel = it

                txtType.text = movieUiModel.type
                Glide.with(imgPoster).load(movieUiModel.posterImgUrl)
                    .fitCenter()
                    .centerCrop()
                    .placeholder(R.drawable.placeholder)
                    .into(imgPoster)

                if (movieUiModel.ratings.isNotEmpty()) {
                    context?.let {
                        movieUiModel.ratings.forEach { ratingUiModel ->
                            val txtView = TextView(it)

                            txtView.text =
                                String.format(
                                    "Rating : %s, Score : %s",
                                    ratingUiModel.source,
                                    ratingUiModel.value
                                )
                            txtView.setTextColor(
                                ContextCompat.getColor(
                                    it,
                                    com.core.common.R.color.white
                                )
                            )

                            linearRating.addView(txtView)
                        }
                    }

                    linearRating.visibility = View.VISIBLE
                }

                txtMetascore.text = String.format("Meta Score : %s", movieUiModel.metaScore)

                txtRating.text = String.format("Imdb Rating : %s", movieUiModel.imgDbRating)
                txtVotes.text = String.format("Imdb Votes : %s", movieUiModel.imdbVotes)
                txtTitle.text = String.format("Title: %s", movieUiModel.title)
                txtYear.text = String.format("Year : %s", movieUiModel.year)
            }
        }

        viewModel.error.observe(viewLifecycleOwner) {
            context?.let { context ->
                Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
            }
        }
    }
}