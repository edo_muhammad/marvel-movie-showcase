package com.movie.showcase.ui.home

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.core.domain.usecase.movieusecase.GetMovieListUseCase
import com.core.ui.model.FeatureMovieUIModel
import com.core.ui.model.UiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val getMovieListUseCase: GetMovieListUseCase
) : ViewModel() {

    private val _featureMovie: MutableLiveData<FeatureMovieUIModel> = MutableLiveData()
    val featureMovie: LiveData<FeatureMovieUIModel> = _featureMovie

    private val _error: MutableLiveData<String> = MutableLiveData()
    val error: LiveData<String> = _error

    val progressVisibility: MutableLiveData<Int> = MutableLiveData(View.VISIBLE)

    fun getMovies() {
        //make launch to run on Main since GetMovieListUseCase already run in IO
        viewModelScope.launch(Dispatchers.Main) {
            getMovieListUseCase.invoke().collect {
                when (it) {
                    is UiState.OnFailed -> {
                        _error.value = it.message
                    }

                    is UiState.OnLoading -> {
                        progressVisibility.value = View.VISIBLE
                    }

                    is UiState.OnSuccess -> {
                        progressVisibility.value = View.GONE

                        _featureMovie.value = it.data
                    }
                }
            }
        }
    }
}