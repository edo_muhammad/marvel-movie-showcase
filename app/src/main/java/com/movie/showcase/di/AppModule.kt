package com.movie.showcase.di

import com.core.common.Constants
import com.movie.showcase.BuildConfig
import com.movie.showcase.core.MovieInterceptor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Provides
    @Named(Constants.MOVIE_BASE_URL)
    fun provideBaseUrl() = BuildConfig.RAPID_API_MOVIE_BASE_URL

    @Named(Constants.MOVIE_OK_HTTP)
    @Singleton
    @Provides
    fun provideOkHttpClient(movieInterceptor: MovieInterceptor) = if (BuildConfig.DEBUG) {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        OkHttpClient.Builder()
            .addInterceptor(movieInterceptor)
            .addInterceptor(loggingInterceptor)
            .build()
    } else {
        OkHttpClient
            .Builder()
            .addInterceptor(movieInterceptor)
            .build()
    }

    @Provides
    @Singleton
    fun provideMovieInterceptor(): MovieInterceptor = MovieInterceptor()

    @Named(Constants.MOVIE_RETROFIT)
    @Singleton
    @Provides
    fun provideRetrofit(
        @Named(Constants.MOVIE_OK_HTTP) okHttpClient: OkHttpClient,
        @Named(Constants.MOVIE_BASE_URL) movieBaseUrl: String
    ): Retrofit = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(movieBaseUrl)
        .client(okHttpClient)
        .build()
}