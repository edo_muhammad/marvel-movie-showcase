#include <jni.h>
#include <string>

extern "C" JNIEXPORT jstring

JNICALL
Java_com_movie_showcase_core_ApiKeys_movieToken(JNIEnv *env, jobject object) {
    std::string api_key = "0d506939cemsh014920d5939c43bp12de22jsn8ec3091c6daf";
    return env->NewStringUTF(api_key.c_str());
}

extern "C" JNIEXPORT jstring

JNICALL
Java_com_movie_showcase_core_ApiKeys_movieApiHost(JNIEnv *env, jobject object) {
    std::string api_key = "movie-database-alternative.p.rapidapi.com";
    return env->NewStringUTF(api_key.c_str());
}

